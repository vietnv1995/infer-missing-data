from __future__ import division
import random, time
from commonAttrib import CommonAttrib
from my_graph import GraphWrapper
import snap
import datetime

class LocallyInfer:
    def __init__(self, G):
        self.G = G
        self.commonAttrib = CommonAttrib(self.G)
        self.last_Eaa = 0
        self.last_Eab = 0
        self.last_Ebb = 0
        self.last_comm = None
        self.V = None

    def evaluate(self, attrib, attribVal):
        # self.V = self.commonAttrib.selectCommunity({attrib: attribVal})
        self.V = self.G.getAllNodesHasAttrib(attrib)
        H = self.commonAttrib.selectCommunity({attrib: attribVal})
        assert len(H) > 1, 'Kich thuoc community < 2 chon comunity khac'
        num_select = int(len(H) * 2 / 3)
        S = random.sample(H, num_select)
        H_S = [item for item in H if item not in S]
        M = [item for item in self.V if item not in H]
        M_sampled = random.sample(M, int(len(H_S)*1.2))
        self.V = H + M_sampled
        B_miss = M_sampled + H_S

        C_begin = self.calculate_C(S)
        C_H = self.calculate_C(H)

        A, R, C = self.detectSingleComm(attrib, attribVal, S=S, B_miss=B_miss)
        RnH = [item for item in H if item in R]
        recall = len(RnH) / len(H_S)
        precision = len(RnH) / len(R)
        return H, R, C_H, C_begin, C, recall, precision

    def calculate_e_AA(self, commA):
        """
        Dem so ket noi ben trong cua community commA
        :param commA: Mang cac node thuoc community A
        :return:
        """
        NIdV = snap.TIntV()
        for i in commA:
            NIdV.Add(i)
        sub_graph = snap.GetSubGraph(self.G.G, NIdV)
        return sub_graph.GetEdges()

    def calculate_e_AB(self, commA):
        result = self.G.get_edges_in_out(commA)
        return len(result)

    def calculate_C(self, commA):
        """
        :param commA: Tap cac not co chung 1 thuoc tinh
        :return: Do do C
        """
        # B = [item.GetId() for item in self.G.nodes() if item.GetId() not in commA]
        e_AA = self.calculate_e_AA(commA)
        e_AB = self.calculate_e_AB(commA)
        e_BB = self.G.getNumOfEdges() - e_AA - e_AB
        e_A = e_AA + e_AB
        e_B = e_BB + e_AB
        assert e_A > 0, 'Khong co ket noi nao tu community, hay chon community khac'
        C = e_AA / (e_AA + e_AB) - e_A * e_B / (e_A * e_A + e_A * e_B)
        # print("e_AA: {}, e_AB: {}, e_A: {}, e_B: {}".format(e_AA, e_AB, e_A, e_B))
        # print C
        return C

    def calculate_next_C(self, newNode):
        neigh = list(self.G.neighbors(newNode))
        inner = [item for item in neigh if item in self.last_comm]
        outer = [item for item in neigh if item not in inner and item in self.V]
        e_AA = self.last_Eaa + len(inner)
        e_AB = self.last_Eab + len(outer) - len(inner)
        e_BB = self.last_Ebb - len(outer)
        e_A = e_AA + e_AB
        e_B = e_BB + e_AB
        C = e_AA / (e_AA + e_AB) - e_A * e_B / (e_A * e_A + e_A * e_B)
        return C, e_AA, e_AB, e_BB

    def detectSingleComm(self, attrib, attribVal, S=None, B_miss=None):
        if S is None:
            S = self.commonAttrib.selectCommunity({attrib: attribVal})
        A = [item for item in S]
        print('Size of the community before infer: {}'.format(len(A)))
        # A_backup = [item for item in A]
        # C = self.calculate_C(A)
        # Ct = C
        if B_miss is None:
            B_miss = self.G.getMissingAttribNodes(attrib)

        loop = 0
        round = len(B_miss)
        max_increase_e_AA = 0
        max_increase_e_AB = 0
        max_increase_e_BB = 0
        C = self.calculate_C(A)
        while True:
            loop += 1
            tic = time.clock()
            # Ct = C
            max_increase = -1
            max_index = 0
            for i in range(len(B_miss)):
                a = B_miss[i]
                # A.append(a)
                tmp_C, e_AA, e_AB, e_BB = self.calculate_next_C(a)
                if tmp_C > max_increase:
                    max_increase = tmp_C
                    max_increase_e_AA = e_AA
                    max_increase_e_AB = e_AB
                    max_increase_e_BB = e_BB
                    max_index = i
                # A.pop()

            if C < max_increase:
                C = max_increase
                a = B_miss[max_index]
                A.append(a)
                B_miss.remove(a)
                self.last_comm = A
                self.last_Eaa = max_increase_e_AA
                self.last_Eab = max_increase_e_AB
                self.last_Ebb = max_increase_e_BB
            else:
                break
            tac = time.clock()
            print('round {}/{}; processing time: {}; current C = {}'.format(loop, round, tac - tic, C))
        infered_nodes = [item for item in A if item not in S]
        return A, infered_nodes, C

    def inferUserAttrib(self, userId, attrib):
        print("{} Start select ego {}".format(datetime.datetime.now(), userId))
        g, df = self.G.select_ego(userId)
        print("{} Done select ego {}".format(datetime.datetime.now(), userId))
        if g is not None:
            egoNet = GraphWrapper(g, df)
            self.G = egoNet
            print 'num of nodes: {}'.format(self.G.getNumOfNodes())
            print 'num of edges: {}'.format(self.G.getNumOfEdges())
            attribVals = self.G.getUniqueAttribVals(attrib)

            # egoNode = egoNet.G.node[userId]
            # if attrib in egoNode:
            #     print('{} of user {}: {}'.format(attrib, userId, egoNode[attrib]))
            commAttrib = CommonAttrib(egoNet)
            comms = commAttrib.select_multi_comunity(attrib)
            max = -1
            max_val = None
            for val, comm in comms.iteritems():
                if len(comm) < 2:
                    continue
                if userId in comm:
                    comm.remove(userId)
                # print("{} Start calculate C".format(datetime.datetime.now()))
                # curr_C = self.calculate_C(comm)
                # print("{} Done calculate C".format(datetime.datetime.now()))
                comm.append(userId)
                # next_C, _, _, _ = self.calculate_next_C(userId)
                # print("{} Start calculate C".format(datetime.datetime.now()))
                next_C = self.calculate_C(comm)
                # print("{} Done calculate C".format(datetime.datetime.now()))
                delta_C = (next_C)
                # delta_C = (next_C - curr_C)  # /abs(curr_C)
                if delta_C > max:
                    max_val = val
                    max = delta_C
                print('{}: {}'.format(val, delta_C))

            # for val in attribVals:
            #     print("{} Start select Community".format(datetime.datetime.now()))
            #     comm = commAttrib.selectCommunity({attrib: val})
            #     print("{} Done select Community".format(datetime.datetime.now()))
            #     if len(comm) < 2:
            #         continue
            #     if userId in comm:
            #         comm.remove(userId)
            #     # print("{} Start calculate C".format(datetime.datetime.now()))
            #     # curr_C = self.calculate_C(comm)
            #     # print("{} Done calculate C".format(datetime.datetime.now()))
            #     comm.append(userId)
            #     # next_C, _, _, _ = self.calculate_next_C(userId)
            #     print("{} Start calculate C".format(datetime.datetime.now()))
            #     next_C = self.calculate_C(comm)
            #     print("{} Done calculate C".format(datetime.datetime.now()))
            #     delta_C = (next_C)
            #     # delta_C = (next_C - curr_C)  # /abs(curr_C)
            #     if delta_C > max:
            #         max_val = val
            #         max = delta_C
            #     print('{}: {}'.format(val, delta_C))

            print('Inferred value: {}'.format(max_val))
            return max_val
        return None

    def infer_userid(self, user_id, attr, ego_user):
        self.G = ego_user
        commAttrib = CommonAttrib(ego_user)
        print("{} Start select comunity {}".format(datetime.datetime.now(), user_id))
        comms = commAttrib.select_multi_comunity(attr)
        print("{} Done select comunity {}".format(datetime.datetime.now(), user_id))
        max = -1
        max_val = None
        for val, comm in comms.iteritems():
            if len(comm) < 2:
                continue
            if user_id in comm:
                comm.remove(user_id)
            # print("{} Start calculate C".format(datetime.datetime.now()))
            # curr_C = self.calculate_C(comm)
            # print("{} Done calculate C".format(datetime.datetime.now()))
            comm.append(user_id)
            # next_C, _, _, _ = self.calculate_next_C(userId)
            # print("{} Start calculate C".format(datetime.datetime.now()))
            next_C = self.calculate_C(comm)
            # print("{} Done calculate C".format(datetime.datetime.now()))
            delta_C = (next_C)
            # delta_C = (next_C - curr_C)  # /abs(curr_C)
            if delta_C > max:
                max_val = val
                max = delta_C
            # print('{}: {}'.format(val, delta_C))
        print('Inferred value: {}'.format(max_val))
        return max_val
