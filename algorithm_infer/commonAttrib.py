# You Are Who You Know: Inferring User Profiles in Online Social Networks
# https://docs.google.com/viewer?url=https%3A%2F%2Fmislove.org%2Fpublications%2FInferring-WSDM.pdf
from community import Community
import numpy as np


class CommonAttrib:
    def __init__(self, graph):
        self.G = graph

    def CalculateSa(self, attribName):
        E = self.G.getNumOfEdges()
        allVals = self.G.getUniqueAttribVals(attribName)
        Eij = 0
        for val in allVals:
            num_i_j = self.G.selectSharedAttribValLinkedNodes(attribName, val)
            Eij += num_i_j
        return Eij / E

    def CalculateEa(self, attribName):
        allVals = self.G.getUniqueAttribVals(attribName)
        SigmaTk = 0
        U = 0
        for val in allVals:
            Ti = len(self.G.getAllSharedAttribValNodes(attribName, val))
            SigmaTk += Ti * (Ti - 1)
            U += Ti
        return SigmaTk / (U * (U - 1))

    def CalculateAa(self, attribName):
        return self.CalculateSa(attribName) / self.CalculateEa(attribName)

    def CalculateModularity(self, attribGroup):
        comms = self.findCommunities(attribGroup)
        comms = list(filter(lambda x: len(x.getAllMember()) > 0, comms))
        comMatrix = np.zeros((len(comms), len(comms)))
        countAllEdges = self.G.getNumOfEdges()
        for i in range(len(comms)):
            for j in range(len(comms)):
                comMatrix[i, j] = round(
                    comms[i].calculateEij(comms[j]) / countAllEdges, 4)
        traceE = np.trace(comMatrix)
        SqureE = np.matmul(comMatrix, comMatrix)
        Q = (traceE - SqureE.sum())
        print(comMatrix.shape)
        print(comMatrix)
        print('Attributes: {}'.format(', '.join(attribGroup)))
        print('Communities: {}'.format(len(comms)))
        print('Modularity: {}'.format(Q))
        return Q

    def findCommunities(self, attribGroup):
        result = []
        for att in attribGroup:
            vals = self.G.getUniqueAttribVals(att)
            if len(result) == 0:
                for val in vals:
                    comm = Community(self.G)
                    comm.filter(att, val)
                    result.append(comm)
            else:
                temp = []
                for val in vals:
                    for comm in result:
                        if att in comm.SharedAttib:
                            cpycomm = comm.deepcopy()
                            cpycomm.filter(att, val)
                            temp.append(cpycomm)
                        elif att not in comm.SharedAttib:
                            comm.filter(att, val)
                for t in temp:
                    result.append(t)
        # print(result)
        return result

    def selectCommunity(self, attribs):
        result = None
        for k, v in attribs.items():
            nodes = self.G.getAllSharedAttribValNodes(k, v, result)
            result = nodes
        return result

    def select_multi_comunity(self, attr_name):
        return self.G.get_community_by_attr_name(attr_name)


