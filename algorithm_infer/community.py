import commonAttrib


class Community:
    def __init__(self, G):
        self.SharedAttib = {}
        self.G = G
        self.Members = None

    def filter(self, attribName, attribVal):
        self.SharedAttib[attribName] = attribVal

    def deepcopy(self):
        comm = Community(self.G)
        for k, v in self.SharedAttib.items():
            comm.filter(k, v)
        return comm

    def getAllMember(self, refresh=False):
        commAtt = commonAttrib.CommonAttrib(self.G)
        if refresh:
            self.Members = commAtt.selectCommunity(self.SharedAttib)
        if self.Members is None:
            self.Members = commAtt.selectCommunity(self.SharedAttib)
        return self.Members

    def calculateEij(self, comm_j):
        myMems = self.getAllMember()
        jMems = comm_j.getAllMember()
        count = 0
        for mymem in myMems:
            # neigh = list(self.G.neighbors(mymem))
            # extra = [item for item in neigh if item in jMems]
            # count += len(extra)
            for jmem in jMems:
                if self.G.has_edge(mymem, jmem):
                    count += 1
        return count


