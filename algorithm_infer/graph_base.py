class GraphBase(object):
    def __init__(self, G=None):
        self.G = G

    def getNumOfEdges(self):
        return 0

    def getNumOfNodes(self):
        return None

    def select_ego(self, egoId):
        # Get subgraph with node ids egoId
        return None

    def selectSharedAttribValLinkedNodes(self, attribName, attribVal, edges=None):
        return None

    def getUniqueAttribVals(self, attribName):

        return None

    def getAllSharedAttribValNodes(self, attribName, attribVal, nodes=None):

        return None

    def getAllNodesHasAttrib(self, attribName):
        return None

    def has_edge(self, nodeid1, nodeid2):
        return False

    def nodes(self):
        return None

    def edges(self):
        return None

    def getMissingAttribNodes(self, attrib):
        return None

    def neighbors(self, nodeid):
        return None

    def FractionRevealed(self):
        return None

    def plot(self):
        return None

    def PrintAttribs(self):
        return None

    def get_edges_in_out(self, node_ids):
        return None