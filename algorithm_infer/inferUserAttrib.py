import datetime
from attribInfer import LocallyInfer
from my_graph import GraphWrapper
import dask.dataframe as dd
import snap
import argparse
from multiprocessing import Pool
import multiprocessing
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument("-e", "--edgefile", help="Path to edge file", default="/dev/data_five9/edges/output3_1000_1199.csv")
parser.add_argument("-n", "--node_property", help="Path to node property", default="/dev/data_five9/data/node_property.csv")
parser.add_argument("-s", "--save_graph", help="Save graph or not", type=int, choices=[0, 1], default=1)
parser.add_argument("-g", "--graph_path", help="Path to graph file", default="/dev/data_five9/graph/graph3_1000_1199.graph")
args = parser.parse_args()

def make_graph(path_node, path_edge, graph_path="", save=False):
    df_attr = dd.read_csv(path_node).compute()
    if save:
        context = snap.TTableContext()
        schema = snap.Schema()
        schema.Add(snap.TStrTAttrPr("srcID", snap.atInt))
        schema.Add(snap.TStrTAttrPr("dstID", snap.atInt))
        sample_table = snap.TTable.LoadSS(schema, path_edge, context, "\t", snap.TBool(False))
        graph = snap.ToGraph(snap.PUNGraph, sample_table, "srcID", "dstID", snap.aaFirst)
        save_graph(graph, graph_path)
    else:
        graph = load_graph(graph_path)
    grapher = GraphWrapper(graph, df_attr)
    return grapher

def save_graph(G, path):
    FOut = snap.TFOut(path)
    G.Save(FOut)
    FOut.Flush()

def load_graph(path):
    FIn = snap.TFIn(path)
    G = snap.TUNGraph.Load(FIn)
    return G

def get_infer_id(path):
    # df = dd.read_csv(path, names=["source", 'target'])
    # df = df.persist()
    df = pd.read_csv(path)
    pre_user_ids = list(df.user_id.tolist())
    return pre_user_ids

def infer_attr_seq(G, user_id, attrib_name):
    infer = LocallyInfer(G)
    infer.inferUserAttrib(user_id, attrib_name)

def infer_user_seq(user_id):
    attribs = ['age', 'hometown', 'location', 'college']
    infer = LocallyInfer(G)
    rs = {}
    rs['user_id'] = user_id
    print("{} Start select ego {}".format(datetime.datetime.now(), user_id))
    g, df = G.select_ego(user_id)
    if g is not None:
        ego_user = GraphWrapper(g, df)
        print 'num of nodes: {}'.format(ego_user.getNumOfNodes())
        print 'num of edges: {}'.format(ego_user.getNumOfEdges())
        print("{} Done select ego {}".format(datetime.datetime.now(), user_id))
        for attrib in attribs:
            r = infer.infer_userid(user_id, attrib, ego_user)
            rs[attrib] = r
        return rs
    else:
        rs['age'] = None
        rs['hometown'] = None
        rs['location'] = None
        rs['college'] = None
        return rs


if __name__ == '__main__':
    # Run with prams python filename.py -s 0
    p_edges = args.edgefile
    p_nodes = args.node_property
    s = args.save_graph
    p_graph = args.graph_path
    if s==1:
        print("{} Start make graph".format(datetime.datetime.now()))
        G = make_graph(p_nodes, p_edges, save=True, graph_path=p_graph)
        print("{} Done make graph".format(datetime.datetime.now()))
    else:
        print("{} Start load graph".format(datetime.datetime.now()))
        G = make_graph(p_nodes, p_edges, graph_path=p_graph)
        print("{} Done load graph".format(datetime.datetime.now()))
    print("{} Start get list infer".format(datetime.datetime.now()))
    user_infer = get_infer_id("/dev/data_five9/edges/user_infers_output3_1000_1199.csv")
    print("{} Done get list infer".format(datetime.datetime.now()))
    print("{} Start infer".format(datetime.datetime.now()))
    number_core = multiprocessing.cpu_count()
    p = Pool(number_core-50)
    # res = infer_user_seq(18154133)
    res = p.map(infer_user_seq, user_infer)
    # print(res)
    df = pd.DataFrame(res)
    df.to_csv("/dev/data_five9/infer_result/infer_output3_1000_1199.csv", index=False)
    print("{} Done infer".format(datetime.datetime.now()))
    # userId = 2
    # attrib = "age"
    # print('args: User: {}; attribute: {}'.format(userId, attrib))
    # infer = LocallyInfer(G)
    # infer.inferUserAttrib(userId, attrib)
