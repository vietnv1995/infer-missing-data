import time
from commonAttrib import CommonAttrib
from attribInfer import LocallyInfer
from my_graph import GraphWrapper

import argparse
parser = argparse.ArgumentParser(description='Process Network.')
parser.add_argument("func", nargs='?',
                    help='FractionRevealed; plot; PrintAttribs; CalcAa; CalcModularity; DetectSingleComm; Evaluate',
                    default="FractionRevealed")
parser.add_argument('attrib', nargs='?',
                    help='attribute name',
                    default='gender')
parser.add_argument('attribVal', nargs='?',
                    help='attribute value',
                    default=77)
args = parser.parse_args()

if __name__ == '__main__':
    G = GraphWrapper()

    attrib = args.attrib
    attribVal = args.attribVal
    print('args: attribute: {}; attribute val: {}'.format(attrib, attribVal))

    if args.func == 'FractionRevealed':
        results = G.FractionRevealed()

        import numpy as np
        import matplotlib.pyplot as plt

        attrs = []
        attrs_re = []
        for k, v in results.items():
            if v > 40:
                attrs.append(k)
                attrs_re.append(v)
        y_pos = np.arange(len(attrs))

        fig = plt.figure(figsize=(11, 5))
        fig.canvas.set_window_title('Revealed ratio')

        plt.bar(y_pos, attrs_re, align='center', alpha=0.5, width=0.4)
        plt.xticks(y_pos, attrs)
        plt.ylabel('Percent')
        plt.title('Revealed ratio')

        plt.show()
    elif args.func == 'plot':
        G.plot()
    elif args.func == 'PrintAttribs':
        G.PrintAttribs()
    elif args.func == 'CalcAa':
        # tic = time.clock()
        # vals = G.getUniqueAttribVals(attrib)
        # toc = time.clock()
        # pt = toc - tic
        # print(len(vals))
        # print(pt)
        # work: 440752
        # print(vals)
        commAttr = CommonAttrib(G)
        print('Aa of {}: {}'.format(attrib, commAttr.CalculateAa(attrib)))
    elif args.func == 'CalcModularity':
        commAttr = CommonAttrib(G)
        tic = time.clock()
        Q = commAttr.CalculateModularity([attrib])
        toc = time.clock()
        print('processing time: {}'.format((toc - tic) / 1))
        print(Q)
    elif args.func == 'DetectSingleComm':
        tic = time.clock()
        locallyInfer = LocallyInfer(G)
        A, infered, C = locallyInfer.detectSingleComm(attrib, attribVal)
        toc = time.clock()
        print('Size of the community after infer: {}'.format(len(A)))
        print('Num of inferred nodes: {}'.format(len(infered)))
        print(infered)
        print('Normalized conductance of the Community: {}'.format(C))
        print('processing time: {}'.format(toc - tic))
    elif args.func == 'Evaluate':
        tic = time.clock()
        locallyInfer = LocallyInfer(G)
        # H, R, C_H, C_begin, C, recall, precision = locallyInfer.evaluate('education_concentration_id', 15)
        H, R, C_H, C_begin, C, recall, precision = locallyInfer.evaluate(
            attrib, attribVal)
        toc = time.clock()
        print('Size of the Community: {}'.format(len(H)))
        print('Size of R: {}'.format(len(R)))
        print('Normalized conductance of the Community: {}'.format(C_H))
        print('Normalized conductance of S: {}'.format(C_begin))
        print('Normalized conductance of S + R: {}'.format(C))
        print('Recall: {}'.format(recall))
        print('Precision: {}'.format(precision))
        print('processing time: {}'.format((toc - tic) / 1))
    elif args.func == 'InferUserAttrib':
        pass