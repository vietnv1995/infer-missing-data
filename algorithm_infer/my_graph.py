import snap
from graph_base import GraphBase


class GraphWrapper(GraphBase):
    def __init__(self, G=None, df_attr=None):
        GraphBase.__init__(self, G)
        self.df_attr = df_attr  # df la dataframe co 5 cot la user_id, face_id, age, hometown, location, college

    def getNumOfEdges(self):
        return int(self.G.GetEdges())

    def getNumOfNodes(self):
        return int(self.G.GetNodes())

    def select_ego(self, egoId):
        neighbors_1 = self.neighbors(egoId)
        # neighbors_2 = self.neighbors(egoId, 2)
        # neighbors_1.extend(neighbors_2)
        neighbors_1.append(egoId)
        if len(neighbors_1) > 1:
            df = self.df_attr[self.df_attr['user_id'].isin(neighbors_1)]
            NIdV = snap.TIntV()
            for i in neighbors_1:
                NIdV.Add(i)
            return snap.GetSubGraph(self.G, NIdV), df
        else:
            return None, None

    def selectSharedAttribValLinkedNodes(self, attribName, attribVal, edges=None):
        """
        Chon tat ca cac cap nut chia se gia tri attribVal cua attribName ma co ket noi voi nhau
        :param attribName:
        :param attribVal:
        :param edges:
        :return:
        """
        count = 0
        node_ids = self.get_nodes_have_attr_val(attribName, attribVal)
        for i in range(len(node_ids)):
            for j in range(len(node_ids) - i):
                if self.has_edge(node_ids[i], node_ids[j]):
                    count += 1
        return count

    def get_nodes_have_attr_val(self, attr_name, attr_val):
        """
        :param attr_name:
        :param attr_val:
        :return: list of node_ids
        """
        # print "dddddddddddddd {}".format(self.df_attr.user_id.size.compute())
        node_ids = self.df_attr[self.df_attr[attr_name]==attr_val]['user_id'].tolist()
        return node_ids

    def get_community_by_attr_name(self, attr_name):
        result = self.df_attr.groupby(attr_name).user_id.apply(list).to_dict()
        return result

    def getUniqueAttribVals(self, attribName):
        return list(self.df_attr[~self.df_attr[attribName].isna()][attribName].unique())

    def getAllSharedAttribValNodes(self, attribName, attribVal, nodes=None):
        """
        lay tat ca cac nut chia se gia tri attriVal cua thuoc tinh attribName, khong quan tam la co ket noi hay ko
        :param attribName:
        :param attribVal:
        :param nodes:
        :return:
        """
        return self.get_nodes_have_attr_val(attribName, attribVal)

    def getAllNodesHasAttrib(self, attribName):
        node_ids = self.df_attr[~self.df_attr[attribName].isna()]['user_id'].tolist()
        return node_ids

    def has_edge(self, nodeid1, nodeid2):
        return self.G.IsEdge(nodeid1, nodeid2)

    def nodes(self):
        return self.G.Nodes()

    def edges(self):
        return self.G.Edges()

    def getMissingAttribNodes(self, attrib):

        result = self.df_attr[self.df_attr[attrib].isnan()]['user_id'].tolist()
        return result

    def neighbors(self, nodeid, deep=1):
        NodeVec = snap.TIntV()
        snap.GetNodesAtHop(self.G, nodeid, deep, NodeVec, False)
        return [item for item in NodeVec]

    def FractionRevealed(self):
        return None

    def plot(self):
        return None

    def PrintAttribs(self):
        return None

    def get_edges_in_out(self, node_ids):
        Nodes = snap.TIntV()
        for node_id in node_ids:
            Nodes.Add(node_id)
        results = snap.GetEdgesInOut(self.G, Nodes)
        return results

# def mem_usage(self, pandas_obj):
#     if isinstance(pandas_obj, DataFrame):
#         usage_b = pandas_obj.memory_usage(deep=True).sum()
#     else:  # we assume if not a df it's a series
#         usage_b = pandas_obj.memory_usage(deep=True)
#     usage_mb = usage_b / 1024**2  # convert bytes to megabytes
#     return "{:03.2f} MB".format(usage_mb)

import dask.dataframe as dd
if __name__ == '__main__':
    # edgefilename = "snap_map_lam_edges.txt"
    edgefilename = "test.txt"
    context = snap.TTableContext()
    schema = snap.Schema()
    schema.Add(snap.TStrTAttrPr("srcID", snap.atInt))
    schema.Add(snap.TStrTAttrPr("dstID", snap.atInt))
    sample_table = snap.TTable.LoadSS(schema, edgefilename, context, "\t", snap.TBool(False))
    graph = snap.ToGraph(snap.PUNGraph, sample_table, "srcID", "dstID", snap.aaFirst)
    df_attr = dd.read_csv('user_info.csv').compute()
    grapher = GraphWrapper(graph, df_attr)
    print(type(grapher.getNumOfEdges()))
