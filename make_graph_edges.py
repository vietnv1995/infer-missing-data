from __future__ import print_function
from __future__ import division
from os import listdir
from os.path import isfile, join
import pandas as pd
import datetime
import numpy as np
import multiprocessing
from multiprocessing import Pool

import pickle

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="Input folder to make graph edges", default="/dev/data_five9/output3")
parser.add_argument("-o", "--output", help="Output folder to make graph edges", default="/dev/data_five9/edges/output3")
args = parser.parse_args()
# print(args.input)

# main_path = '/dev/data_five9/output3'
# main_out_path = '/dev/data_five9/edges/output3'


def load_meta_data(path):
    with open(path, 'rb') as handle:
        d_id_to_int = pickle.load(handle)
    return d_id_to_int

def make_df(file_name):
    try:
        df = pd.read_csv(join(main_path, file_name), sep="\t", names=['user_id', 'friend_ids'], dtype='str')
        df = my_func(df.loc[(~df['user_id'].isna()) & (~df['friend_ids'].isna()),:])
        df = df.loc[(~df['source'].isna()) & (~df['friend_ids'].isna()),:]
        print("{} Start make edges for file {}".format(datetime.datetime.now(), file_name))
        my_dict = df.to_dict('list')
        source_ids = ",".join(my_dict['source']).split(",")
        target_ids = ",".join(my_dict['friend_ids']).split(",")
        df = pd.DataFrame({'source': source_ids, 'target': target_ids})
        # df['check'] = df.apply(
        #     lambda row: str(row['source']) + "-" + str(row['target']) if row['source'] < row['target'] else str(
        #         row['target']) + "-" + str(row['source']), axis=1)
        # df.drop_duplicates('check', inplace=True)
        # df = df[['source', 'target']]
        df.to_csv(join(main_out_path, file_name+".csv"), sep='\t', index=False, header=False)
        del df
        print("{} Done make edges for file {}".format(datetime.datetime.now(), file_name))
    except Exception as e:
        print("{} is wrong".format(file_name))
        print(e)



def my_func(df):
    df = df.apply(export_edges, axis=1)
    df_edges = df[['source', 'friend_ids']]
    return df_edges

def export_edges(df):
    if 'friend_ids' in df and 'user_id' in df:
        if pd.notnull(df['friend_ids']) and pd.notnull(df['user_id']):
            user_id = np.int64(df['user_id'])
            if user_id in d:
                user_id = d[user_id]
                friend_ids = df['friend_ids'].split(",")
                if isinstance(friend_ids, list):
                    tmp = []
                    for item in friend_ids:
                        if np.int64(item) in d:
                            tmp.append(str(d[np.int64(item)]))
                    if len(tmp) > 0:
                        sources= (str(user_id) + ",")*len(tmp)
                        df['source'] = sources[:-1]
                        df['friend_ids'] = ",".join(tmp)
                else:
                    df['source'] = None
            else:
                df['source'] = None
        else:
            df['source'] = None
    else:
        df['source'] = None
    return df

def parallelize_dataframe(file_names, func):
    num_cores = multiprocessing.cpu_count()
    pool = Pool(num_cores-50)
    pool.map(func, file_names)
    pool.close()
    pool.join()


if __name__ == '__main__':
    main_path = args.input
    main_out_path = args.output
    file_names = listdir(main_path)
    print("{} Start load file meta data".format(datetime.datetime.now()))
    d = load_meta_data('/dev/data_five9/data/meta_id_to_int.pickle')
    print("{} Done load file meta data".format(datetime.datetime.now()))
    df_result = parallelize_dataframe(file_names, make_df)


