import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i","--input", help="Path to input folder concat", default="/dev/data_five9/edges/output3/part-0")
parser.add_argument("-o", "--output", help="Path to output file concat", default="/dev/data_five9/edges/output3_1000_1199.csv")
parser.add_argument("-r", "--regex", help="Regex to filter filename", default="1[01]*")
args = parser.parse_args()

if __name__ == "__main__":
    p_input = args.input
    p_out = args.output
    regex = args.regex
    p_input = p_input + regex
    script = "for file in "+p_input+"; do cat $file >> " + p_out +"; done"
    subprocess.call(script, shell=True)
    subprocess.call("wc -l "+p_out, shell=True)